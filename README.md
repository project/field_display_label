# Field Display Label

This module provides a different label for displaying fields from the label 
used when viewing the field in a form. For example, if you want the field to 
display label "Body" when editing content, but to display label "Body Display"
when viewing the content.

## CONTENTS OF THIS FILE

 * Requirements
 * Installation
 * Configuration
 * Maintainers

## REQUIREMENTS

This module requires Core Field.

## INSTALLATION

Install as normal (see http://drupal.org/documentation/install/modules-themes).

## CONFIGURATION

1. Go to Administration/Structure/Content types/YOU CONTENT TYPE/Manage fields.
2. Edit any field.
3. Fill in "Display label" with displaying label.

## MAINTAINERS

Neslee Canil Pinto: https://www.drupal.org/u/neslee-canil-pinto
plazik: https://www.drupal.org/u/plazik
Stephen Mustgrave: https://www.drupal.org/u/smustgrave